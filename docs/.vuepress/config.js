module.exports = {
  title: 'Guia de Sierra Nevada',
  description: 'Guia completa para Sierra Nevada',
  head: [
    ['link', { rel: 'manifest', href: '/manifest.json' }]
  ],
  plugins: ['@vuepress/pwa'],
  locales: {
    '/': {
      lang: 'es-ES', // this will be set as the lang attribute on <html>
      title: 'Guia de Sierra Nevada',
      description: 'Guia completa para Sierra Nevada'
    }
  },
  themeConfig: {
    docsDir: 'docs',
    repo: 'https://gitlab.com/raul.emo/guia-sierra-nevada',
    repoLabel: 'Colabora',
    sidebar: [
      ['/', 'Inicio'],
      '/prologo',
      '/inicio'
    ],
    nav: [
      {
        text: 'Admin',
        link: '/admin/#/',
      }
    ],
    lastUpdated: 'Ultima actualización', // string | boolean
    plugins: {
   '@vuepress/pwa': {
      serviceWorker: true,
      updatePopup: {
        message: "Nuevo contenido esta disponible.",
        buttonText: "Recargar"
        }
      }
    }
  }
  
}