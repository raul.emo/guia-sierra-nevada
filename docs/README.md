---
home: true 
heroImage: /images/icon_256.png
actionText: Comenzar →
actionLink: /inicio
features:
- title: Buscador integrado 
  details: Para encontrar la información rápidamente.
- title: Compatible con móviles 
  details: Compatible con pantallas de móviles. 
- title: Disponible sin conexión
  details: Para consultarlo cuando no hay cobertura móvil. 
footer: Licencia CC BY-SA |
---
